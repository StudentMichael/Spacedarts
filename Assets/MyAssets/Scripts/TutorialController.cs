﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{

    [SerializeField]
    GameObject tutorialLady;

    public void Disable()
    {
        tutorialLady.SetActive(false);
    }
}
