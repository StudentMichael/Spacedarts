﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Function of gravity calculated using Newton's law of universal gravitation, so F = G*(M*m)/r^2
//https://courses.lumenlearning.com/boundless-physics/chapter/newtons-law-of-universal-gravitation

public class Attractor : MonoBehaviour
{
    public Rigidbody rb;
    const float G = 6.674f;
    private GameObject rocket;
    private bool isObjThrown;

    private void Start()
    {
        rocket = GameObject.FindGameObjectWithTag("Rocket");
    }

    private void FixedUpdate()
    {
        isObjThrown = rocket.GetComponent<rocketController>().isThrown;
        if (isObjThrown)
        {
            Attract(rocket);
        }
    }

    void Attract(GameObject objToAttract)
    { 
        Rigidbody rbToAttract = objToAttract.GetComponent<Rigidbody>();

        Vector3 direction = rb.position - rbToAttract.position;
        float distance = direction.magnitude;

        float forceMagnitude = G * (rb.mass * rbToAttract.mass) / Mathf.Pow(distance, 2);
        Vector3 force = direction.normalized * forceMagnitude;

        rbToAttract.AddForce(force);
    }
}
