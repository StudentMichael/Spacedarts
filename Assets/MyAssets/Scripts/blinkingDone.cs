﻿using UnityEngine;

public class blinkingDone : MonoBehaviour
{
    [SerializeField]
    private GameObject mouth;
    public bool animDone = false;

    private void Update()
    {
        if(animDone)
            mouth.GetComponent<EatingController>().confirmSceneSwitch();
    }
}
