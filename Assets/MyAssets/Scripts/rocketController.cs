﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rocketController : MonoBehaviour
{

    //isThrown = false disables attraction by planets
    public bool isThrown;
    [SerializeField]
    private Rigidbody rb;
    private Vector3 startLocation;
    private Quaternion startRotation;

    [SerializeField]
    private GameObject explosionParticle;
    [SerializeField]
    private GameObject goalParticle;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip explosionClip;
    [SerializeField]
    private AudioClip goalClip;
    [SerializeField]
    private AudioClip returnClip;

    // Start is called before the first frame update
    void Start()
    {
        isThrown = false;
        Physics.gravity = Vector3.zero;
        startLocation = rb.transform.position;
        startRotation = rb.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if ((OVRInput.Get(OVRInput.Button.PrimaryHandTrigger) 
            || OVRInput.Get(OVRInput.Button.SecondaryHandTrigger)) 
            && transform.GetComponent<OVRGrabbable>().isGrabbed)
        {
            Debug.LogError("Got here");
            //clear previous trails
            foreach (TrailRenderer trail in FindObjectsOfType<TrailRenderer>())
            {
                trail.Clear();
            }
            //Enables physics
            isThrown = true;
        }
        //B button resets
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            ResetPosition("resetButton");
        }
    }

    public void ResetPosition(string trigger)
    {
        //Based on what triggered reset, play different animation + sound
        switch(trigger)
        {
            //Particles automatically destroy itself upon completion
            case "goal":
                Instantiate(goalParticle, transform.position, transform.rotation);
                audioSource.transform.position = startLocation;
                audioSource.PlayOneShot(goalClip);
                break;
            case "planet":
                Instantiate(explosionParticle, transform.position, transform.rotation);
                audioSource.transform.position = transform.position;
                audioSource.PlayOneShot(explosionClip);
                break;
            case "resetButton":
                audioSource.transform.position = transform.position;
                audioSource.PlayOneShot(returnClip);
                break;
        }
        
        //Reset position
        rb.transform.position = startLocation;
        rb.transform.rotation = startRotation;

        //Remove all acceleration
        rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.constraints = RigidbodyConstraints.None;
        isThrown = false;
    }
}
