﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Function of gravity calculated using Newton's law of universal gravitation, so F = G*(M*m)/r^2
//https://courses.lumenlearning.com/boundless-physics/chapter/newtons-law-of-universal-gravitation

public class planetController : MonoBehaviour
{
    //rotation
    [SerializeField]
    private int speed;

    //Attraction
    [SerializeField]
    private Rigidbody rb;
    const float G = 6.674f;
    private GameObject rocket;
    private bool isObjThrown;


    private void Start()
    {
        rocket = GameObject.FindGameObjectWithTag("Rocket");
    }

    void Update()
    {
        // Spinning planet
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }


    private void FixedUpdate()
    {
        isObjThrown = rocket.GetComponent<rocketController>().isThrown;
        if (isObjThrown)
        {
            Attract(rocket);
        }
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("Rocket"))
        {
            coll.gameObject.GetComponent<rocketController>().ResetPosition("planet");
        }
    }

    void Attract(GameObject objToAttract)
    {
        Rigidbody rbToAttract = objToAttract.GetComponent<Rigidbody>();

        Vector3 direction = rb.position - rbToAttract.position;
        float distance = direction.magnitude;

        float forceMagnitude = G * (rb.mass * rbToAttract.mass) / Mathf.Pow(distance, 2);
        Vector3 force = direction.normalized * forceMagnitude;

        rbToAttract.AddForce(force);
    }
}
