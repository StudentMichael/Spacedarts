﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class lineController : MonoBehaviour
{

    [SerializeField]
    private GameObject stringStart;
    [SerializeField]
    private GameObject stringEnd;
    [SerializeField]
    private LineRenderer lineRenderer;
    [SerializeField]
    private GameObject tutorialLady;
    private Vector3 originalPosition;

    private void Start()
    {
        originalPosition = stringEnd.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, stringStart.transform.position);
        lineRenderer.SetPosition(1, stringEnd.transform.position);
        if(Vector3.Distance(stringStart.transform.position, stringEnd.transform.position) >= 0.8)
        {
            //let go of object and reset string
            stringEnd.GetComponent<OVRGrabbable>().enabled = false;
            stringEnd.transform.position = originalPosition;
            lineRenderer.SetPosition(1, stringEnd.transform.position);
            stringEnd.GetComponent<OVRGrabbable>().enabled = true;

            //Trigger tutorial or back to starting scene
            if (stringEnd.gameObject.CompareTag("TutorialTrigger"))
            {
                tutorialLady.SetActive(true);
            } else if (stringEnd.gameObject.CompareTag("BackToStartingSceneTrigger"))
            {
                string sceneToLoad = "MyAssets/Scenes/openingScene";

                AsyncOperation loadingOperation = SceneManager.LoadSceneAsync(sceneToLoad);

            }
        }
    }
}
