﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighscoreTable : MonoBehaviour
{

    [SerializeField]
    private string level;
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<Transform> highscoreEntryTransformList;
    
    private void Awake()
    {
        entryContainer = transform.Find("HighscoreEntryContainer");
        entryTemplate = entryContainer.Find("HighscoreEntryTemplate");

        LoadHighscores();
    }

    private void LoadHighscores()
    {
        //Every reload, unload all old children
        //Not destroying because the entryTemplate needs to be kept
        //TODO: Delete all except for entryTemplate
        foreach (Transform child in entryContainer)
        {
            child.gameObject.SetActive(false);
        }

        //Retrieve from PlayerPrefs the highscore JSON, if this is empty generate new ones.
        string jsonString = PlayerPrefs.GetString("highscoreTable" + level);
        if (jsonString == null || jsonString == "")
        {
            InitHighscores(level);
            jsonString = PlayerPrefs.GetString("highscoreTable" + level);
        }
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        // Sorting
        highscores.highscoreEntryList.Sort((highscore1, highscore2) => highscore2.score.CompareTo(highscore1.score));

        //Generate entries in screenspace
        highscoreEntryTransformList = new List<Transform>();
        foreach (HighscoreEntry highscoreEntry in highscores.highscoreEntryList)
        {
            CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
        }
    }


    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
        float singleScoreHeight = 30f;

        Transform entryTransform = Instantiate(entryTemplate, entryContainer);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -singleScoreHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;

        string rankString;

        switch (rank)
        {
            case 1: 
            case 8: 
                rankString = rank + "STE"; break;
            default: rankString = rank + "DE"; break;
        }

        //Setting of texts
        entryTransform.Find("PosText").GetComponent<TextMeshProUGUI>().text = rankString;
        entryTransform.Find("ScoreText").GetComponent<TextMeshProUGUI>().text = highscoreEntry.score.ToString();
        entryTransform.Find("NameText").GetComponent<TextMeshProUGUI>().text = highscoreEntry.name;

        //Backgrounds
        entryTransform.Find("Background").gameObject.SetActive(rank % 2 == 1);

        //First place is shiny $$$
        if (rank == 1)
        {
            entryTransform.Find("PosText").GetComponent<TextMeshProUGUI>().color = Color.yellow;
            entryTransform.Find("ScoreText").GetComponent<TextMeshProUGUI>().color = Color.yellow;
            entryTransform.Find("NameText").GetComponent<TextMeshProUGUI>().color = Color.yellow;
        }
        transformList.Add(entryTransform);
    }

    public void AddHighscoreEntry(int score, string name, string levelToUpdate)
    {
        // Create entry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };

        // Load saved highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable" + levelToUpdate);
        Highscores highscores;
        if (jsonString == null || jsonString == "")
        {
            highscores = new Highscores();
        }
        else
        {
            highscores = JsonUtility.FromJson<Highscores>(jsonString);
        }

        // Add new entry to List but limit to 10 highest
        highscores.highscoreEntryList.Add(highscoreEntry);
        if(highscores.highscoreEntryList.Count > 10)
        {
            highscores.highscoreEntryList.Sort((highscore1, highscore2) => highscore2.score.CompareTo(highscore1.score));
            highscores.highscoreEntryList.RemoveAt(highscores.highscoreEntryList.Count - 1);
            Debug.Log("Max 10 highscores, removed entry 11");
        }

        // Save updated highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable" + levelToUpdate, json);
        PlayerPrefs.Save();

        LoadHighscores();
    }

    //Init if there's no highscores yet
    private void InitHighscores(string level)
    {
        AddHighscoreEntry(1000, "Laika", level);
        AddHighscoreEntry(3333, "Chris Hadfield", level);
        AddHighscoreEntry(4000, "Sally Ride", level);
        AddHighscoreEntry(6000, "Neil Armstrong", level);
        AddHighscoreEntry(7000, "Buzz Lightyear", level);
        AddHighscoreEntry(9000, "Gagarin", level);
    }


    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList = new List<HighscoreEntry>();
    }

    [System.Serializable]
    private class HighscoreEntry
    {
        public int score;
        public string name;
    }
}
