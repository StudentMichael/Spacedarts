﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EatingController : MonoBehaviour
{

    [SerializeField]
    private AudioSource eatingSound;
    [SerializeField]
    private GameObject blinkBottom;
    [SerializeField]
    private GameObject blinkTop;
    private AsyncOperation loadingOperation;

    private void OnTriggerEnter(Collider coll)
    {
        Debug.Log(coll.gameObject.tag);
        if (coll.gameObject.CompareTag("Edible"))
        {
            eatingSound.Play();
            //Start blinking animation
            blinkBottom.GetComponent<Animator>().SetBool("PlayAnimation", true);
            blinkTop.GetComponent<Animator>().SetBool("PlayAnimation", true);

            string sceneName = coll.gameObject.GetComponent<EdibleController>().getSceneName();
            string sceneToLoad = "MyAssets/Scenes/" + sceneName;

            loadingOperation = SceneManager.LoadSceneAsync(sceneToLoad);

            //Destroy eaten object
            Destroy(coll.gameObject);
            //Wait until blinking animation is done
            loadingOperation.allowSceneActivation = false;

        }
    }

    //Gets triggered when BottomBlink animation is done
    internal void confirmSceneSwitch()
    {
        loadingOperation.allowSceneActivation = true;
    }
}
