﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class goalController : MonoBehaviour
{
    [SerializeField]
    GameObject goalCenter;
    [SerializeField]
    string levelToUpdate;
    [SerializeField]
    GameObject highscoreTable;

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("Rocket"))
        {
            Vector3 rocketPosition = coll.gameObject.transform.position;

            float zDistance = Mathf.Abs(rocketPosition.z - goalCenter.transform.position.z);
            float yDistance = Mathf.Abs(rocketPosition.y - goalCenter.transform.position.y);

            float total = zDistance + yDistance;

            //Score minstens 1000, des te verder van het midden, des te lager de score
            int score = (int) (1000 + 9000 * (1 - total/11));
            
            highscoreTable.GetComponent<HighscoreTable>().AddHighscoreEntry(score, "Player1", levelToUpdate);
            GameObject notificationBar = GameObject.FindGameObjectWithTag("Notification");
            notificationBar.GetComponent<TextMeshProUGUI>().SetText(string.Concat("Your latest score was ", score.ToString()));
            Invoke("ClearScoreText", 7.0f);
            coll.gameObject.GetComponent<rocketController>().ResetPosition("goal");
        }
    }

    private void ClearScoreText()
    {
        GameObject notificationBar = GameObject.FindGameObjectWithTag("Notification");
        notificationBar.GetComponent<TextMeshProUGUI>().SetText("");
    }

    private void ResetRocket()
    {

    }


}
